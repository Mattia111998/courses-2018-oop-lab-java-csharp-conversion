﻿using System;
using System.Linq;

namespace Unibo.Oop.Utils
{
    internal class TupleImpl : ITuple
    {
        private readonly object[] items;

        public TupleImpl(object[] args)
        {
            this.items = (object[])args.Clone();
        }

        public object this[int i] => items[i];

        public int Length => items.Length;

        public object[] ToArray()
        {
            return (object[])items.Clone();
        }

        public override bool Equals(object obj)
        {
            if(obj != null && obj is TupleImpl)
            {
                TupleImpl tuple = (TupleImpl)obj;
                object[] newArray = tuple.ToArray();
                if(items.Length.Equals(newArray.Length))
                {
                    for(int i = 0; i < items.Length; i++)
                    {
                        if(!items[i].Equals(newArray[i]))
                        {
                            return false;
                        }
                    }
                    return true;
                }
            }
            return false;
        }

        public override int GetHashCode()
        {
            int prime = 31;
            int result = 1;
            foreach (object obj in items)
            {
                result = prime * result + obj.GetHashCode();
            }
            return result;
        }

        public override string ToString()
        {
            string str = "(";
            int i = 0;
            foreach (var o in items)
            {
                if(i == items.Length - 1)
                {
                    str += o;
                }
                else
                {
                    str += o + ", ";
                    i++;
                }     
            }
            return str += ")";
        }
    }
}
