﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Unibo.Oop.Events
{
    class OrderedEvent<TArg> : AbstractEventSource<TArg>
    {
        private List<EventListener<TArg>> eventListener = new List<EventListener<TArg>>();

        protected override ICollection<EventListener<TArg>> getEventListeners()
        {
            return eventListener;
        }
    }
}
