﻿using System;
namespace Unibo.Oop.Events
{
    public static class EventEmitter
    {
        public static IEventEmitter<TArg> Ordered<TArg>() 
        {
            return new OrderedEvent<TArg>();
        }

        public static IEventEmitter<TArg> Unordered<TArg>()
        {
            return new UnorderedEvent<TArg>();
        }
    }
}
