﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unibo.Oop.Events
{
    class UnorderedEvent<TArg> : AbstractEventSource<TArg>
    {
        private ISet<EventListener<TArg>> eventListener = new HashSet<EventListener<TArg>>();
        protected override ICollection<EventListener<TArg>> getEventListeners()
        {
            return eventListener;
        }
    }
}
