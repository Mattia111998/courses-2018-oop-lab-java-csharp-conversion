﻿using System;
using System.Collections.Generic;
using System.Collections;
using Unibo.Oop.Events;

namespace Unibo.Oop.Events
{
    abstract class AbstractEventSource<TArg> : IEventSource<TArg>, IEventEmitter<TArg>
    {
        protected abstract ICollection<EventListener<TArg>> getEventListeners();

        public void Bind(EventListener<TArg> eventListener)
        {
            getEventListeners().Add(eventListener);
        }

        public void Unbind(EventListener<TArg> eventListener)
        {
            getEventListeners().Remove(eventListener);
        }

        public void UnbindAll()
        {
            getEventListeners().Clear();
        }

        public void Emit(TArg data)
        {
            
        }

        public IEventSource<TArg> EventSource => this;
    }
}
